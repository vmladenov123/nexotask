/**
 * 
 */
class BookStoreAppPage {
	constructor() {
		
	}
	
	clickLoginButton() {
		cy.get("#login").click();
	}
	
	login(username, password) {
		this.clickLoginButton();
		cy.get("#userName").type(username);
		cy.get("#password").type(password);
		this.clickLoginButton();
	}
	
	verifyUserValue(username) {
		cy.get('#userName-value').then(($value) => {
			  const txt = $value.text()
			  expect(txt).to.equal(username)
			})
	}
	
	searchBook(bookName) {
		cy.get('#searchBox').type(bookName)
	}
	
	verifyBookIsFound(bookName) {
		cy.xpath("//a[contains(text(), '" + bookName + "')]").then(($value) => {
			  const txt = $value.text()
			  expect(txt).to.contains(bookName)
			})
	}
}
export default BookStoreAppPage;