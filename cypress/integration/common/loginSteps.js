import { Given , And , Then , When} from "cypress-cucumber-preprocessor/steps";
import HomePage from '../Pages/HomePage';
import BookStoreAppPage from '../Pages/BookStoreAppPage';

const home = new HomePage();
const bookStoreApp = new BookStoreAppPage();

Given('I open the test page {string}', (appUrl) => {            
	cy.visit(appUrl); 
}
); 

When('Perform Login user: {string} password: {string}', (userName, password) => {            
	home.goToBookStorApp();
	bookStoreApp.login(userName, password);
}
);

Then('Verify user is logged {string}', (userName) => {            
	bookStoreApp.verifyUserValue(userName);
}		
);