import { Given , And , Then , When} from "cypress-cucumber-preprocessor/steps";
import BookStoreAppPage from '../Pages/BookStoreAppPage';

const bookStoreApp = new BookStoreAppPage();

When('Search desired book {string}', (bookName) => {            
	bookStoreApp.searchBook(bookName);
}
);

Then('Verify book is found {string}', (bookName) => {            
	bookStoreApp.verifyBookIsFound(bookName);
}		
);
