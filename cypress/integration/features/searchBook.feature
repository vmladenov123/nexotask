Feature: Login in Test application and search desired book
  Scenario: Search book
  Given I open the test page 'https://demoqa.com/'
  And Perform Login user: 'vladimir.mladenov' password: 'NexoTest123@'
  When Search desired book 'ECMAScript 6'
  Then Verify book is found 'ECMAScript 6'